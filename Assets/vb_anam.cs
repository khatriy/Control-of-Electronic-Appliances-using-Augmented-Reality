﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Vuforia;
using System;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;

public class vb_anam : MonoBehaviour,IVirtualButtonEventHandler {
	public GameObject vbBtnObject;
	public MqttClient client;
	public Boolean buttonPressed = false;
	// Use this for initialization
	void Start () {
		vbBtnObject = GameObject.Find ("CntrlBtn");
		vbBtnObject.GetComponent<VirtualButtonBehaviour> ().RegisterEventHandler (this);
		Debug.Log("creating broker");
		this.client = new MqttClient("iot.eclipse.org"); 
		this.client.MqttMsgPublished += client_MqttMsgPublished;
		string clientId = Guid.NewGuid().ToString(); 
		client.Connect(clientId); 
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void OnButtonPressed(VirtualButtonBehaviour vb){
		if (this.buttonPressed == false) {
			this.buttonPressed = true;
			Debug.Log ("button pressed");
			Debug.Log ("publishing message");
			Debug.Log ("sending...");
			this.client.Publish ("topic/testyash", System.Text.Encoding.UTF8.GetBytes ("1"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			Debug.Log("button has already been pressed, release it first");
		}

	}
	public void OnButtonReleased(VirtualButtonBehaviour vb){
		Debug.Log ("button released");
		this.buttonPressed = false;

	}
	public void client_MqttMsgPublished(object sender, MqttMsgPublishedEventArgs e)
	{
		Debug.Log("MessageId = " + e.MessageId);
		Debug.Log("sent");
	}

}
